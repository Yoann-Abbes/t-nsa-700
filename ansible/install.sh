tar -cvf ../front.tar -C ../front/dist/front .
ansible-playbook -i inventory.cfg playbooks/install_front.yml
tar -cvf ../back.tar -C ../back/ .
ansible-playbook -i inventory.cfg playbooks/install_back.yml